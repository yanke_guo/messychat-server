#!/bin/bash

#Env
NODE_PATH="/usr/local/bin"
NODE_COMMAND="node --harmony"
NODE_SCRIPT="$(pwd)/boot.js"

export NODE_ENV=production
export PATH=$NODE_PATH:$PATH

#Exec

forever start -c "$NODE_COMMAND" $NODE_SCRIPT
