'use strict';

var fs = require('fs');

module.exports = {
  redis: {
    host: 'localhost',
    port: 6379
  },
  mongoUrl: "mongodb://localhost/messy",
  server: {
    port: 4000
  },
  messy: {
  },
  apn: {
    pfx: fs.readFileSync(__dirname + '/apn.p12'),
    passphrase: 'qwerty'
  }
};
