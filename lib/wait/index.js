'use strict';

module.exports = function(ms) {
  return function(done) {
    setTimeout(done, ms);
  };
};
