'use strict';

var Router  =   require('koa-router');

var web = new Router();

web.get('/', function * () {
  yield this.render('index',{}, true);
});

web.redirect('/index.html','/');

module.exports = web;
