'use strict';

var koaMount    =   require('koa-mount');
var apiExtend   =   require('./concern/apiExtend');

var web = require('./web');
var api = require('./api');

module.exports = function(app) {
  // mount web
  app.use(koaMount('/',     web.middleware()));
  // mount api
  app.use(apiExtend());
  app.use(koaMount('/api',  api.middleware()));
  app.use(koaMount('/api',  function * () {
    this.error(999, 'api not found');
  }));
};
