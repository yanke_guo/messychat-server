'use strict';

var Router      = require('koa-router');
var config      = require('../../config');
var users       = require('./users');
var tokens      = require('./tokens');
var messages    = require('./messages');

var needLogin       = require('../concern/needLogin');
var rateLimit       = require('../concern/rateLimit');
var needLogin       = require('../concern/needLogin');
var needBody        = require('../concern/needBody');
var needBodyAuth    = require('../concern/needBodyAuth');
var userBasedLock   = require('../concern/userBasedLock');

var api = new Router();

api.post('/users',
    needBodyAuth({ email: true }),
    users.create);

api.get( '/users/current',
    needLogin(),
    users.showMe);

api.post('/users/current/auths',
    needLogin(),
    needBodyAuth({ email: true }),
    users.addAuth);

api.post('/tokens',
    needBodyAuth({ email: true }),
    tokens.create);

api.put( '/users/current',
    needBody(),
    needLogin(),
    users.update);

api.del( '/tokens/:token_id',
    needLogin(),
    tokens.destroy);

api.post('/oauth',
    needBodyAuth( { facebook: true }),
    users.oauth);

api.post('/messages',
    needBody('content','location'),
    needLogin(),
    messages.create);

api.get( '/messages/:message_id',
    needLogin(),
    messages.show);

module.exports = api;
