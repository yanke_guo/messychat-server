'use strict';

var messages = module.exports;
var _        = require('lodash');

messages.create = function * () {
  var ApnMessage = this.ApnMessage;
  var Message = this.Message;
  var User    = this.User;

  var user    = this.currentUser;
  var params  = _.pick(this.request.body, 'content', 'location');
  params.location = _.map(params.location.split(','), function(val) { return parseFloat(val); });
  if (params.location.length != 2) {
    return this.error(2, 'format location is wrong');
  }
  params.user_id  = user._id;
  var apn = this.apn;
  var message = yield Message.create(_.merge(params,{ created_at: Date.now()}));
  if (message) {
    User.find({
      location: {
                  $geoWithin: {
                    $center: [ params.location, 0.008 ]
                  }
                }
    },'apn_token', function(err,users) {
      var note = new ApnMessage({ message_id: message.id, push_type: 'new_message' });
      note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now
      _.each(users,function(user) { 
        if (user.apn_token && user.apn_token.length > 0) {
          apn.pushNotification(note, user.apn_token); 
        }
      });
    });
  }

  var messageJSON = message.toJSON();
  messageJSON['user'] = user.toJSON();
  this.body = { message: messageJSON };
};

messages.show   = function * () {
  if (! this.params.message_id) return this.error(1, 'message not found');
  var cacheKey = "mc.messages." + this.params.message_id;
  var cached = yield this.coredis.get(cacheKey);
  if (!cached) {
    var Message = this.Message;
    var User    = this.User;
    var message = yield Message.findOne({ _id: this.params.message_id }).exec();
    if (! message) return this.error(2, 'message not found');
    var user        = yield User.findOne({ _id: message.user_id }).exec();
    var messageJSON = _.pick(message.toJSON(), '_id','content', 'created_at');
    if (user) messageJSON['user'] = _.pick(user.toJSON(), '_id','nickname', 'avatar_url');
    var body = { message: messageJSON };
    this.redis.setex(cacheKey, 3600, JSON.stringify(body));
    this.body = body;
  } else {
    try {
      this.body = JSON.parse(cached);
    } catch (e) {
      this.redis.del(cacheKey);
      this.error();
    }
  }
};
