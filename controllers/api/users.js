'use strict';

var sha1    = require('sha1');
var _       = require('lodash');
var vali    = require('validator');
var d       = require('debug')('messy');
var random  = require('../../lib/random');
var wait    = require('../../lib/wait');

var users = module.exports = exports

users.oauth = function * () {
  var Auth  = this.Auth;
  var Token = this.Token;
  var User  = this.User;

  var auth = yield Auth.findOne(_.pick(this.authParams, 'provider','identifier')).exec();
  var user = null;

  if (auth) {
    user = yield User.findOne({ _id: auth.user_id }).exec();
    if (!user) {
      auth.remove();
      auth = null;
    }
  }

  if (!auth) {
    auth = yield Auth.create(this.authParams);
    user = yield User.create(_.pick(this.request.body, 'avatar_url','nickname'));
    auth.user_id = user._id;
    yield auth.save.bind(auth);
  }

  var token = yield Token.create({ token: random(64), user_id: user._id });

  this.session.token = token.token;

  this.body = { user: user.toJSON(), token: token.toJSON() };
};

users.showMe  = function * () {
  this.body = { user: this.currentUser.toJSON() };
};

users.addAuth = function * () {
  var User = this.User;
  var Auth = this.Auth;

  var user = this.currentUser;
  var authParams = _.merge(this.authParams, { user_id: user._id });
  var auth = yield Auth.findOne(_.pick(authParams, 'identifier', 'user_id')).exec();

  if (auth) { 
    this.error(104, 'already has a auth with ' + auth.provider); 
  } else {
    try { 
      auth = yield Auth.create(authParams); 
    } catch (ex) {
      if (ex.code == 11000) return this.error(105, 'duplicated identifier'); else return this.error();
    }
    this.body = _.pick(auth.toJSON(),'_id','provider','identifier','user_id');
  }
};

users.update = function * () {
  var user = this.currentUser;
  if (this.request.body.apn_token)  user.apn_token = this.request.body.apn_token;
  if (this.request.body.nickname)   user.nickname = this.request.body.nickname;
  if (this.request.body.location)   user.location = _.map(this.request.body.location.split(','),function(val){
    return parseFloat(val);
  });
  user.save();
  this.status = 200;
  this.body = { user: user.toJSON() };
};

users.create = function * () {
  var User = this.User;
  var Token= this.Token;
  var Auth = this.Auth;
  var authParams = this.authParams;

  var user = null;
  var user = yield User.create(_.pick(this.request.body, 'avatar_url','nickname'));
  var auth = null;

  if (authParams) {
    try {
      auth = yield Auth.create(_.merge(authParams, { user_id: user._id }));
    } catch (e) {
      user.remove();
      if(e.code == 11000) return this.error(105, 'duplicated identifier'); else return this.error();
    }
  }

  var token = yield Token.create({ token: random(64), user_id: user._id });

  this.session.token = token.token;
  this.status = 200;
  this.body = { user: user.toJSON(), token: token.toJSON() };
};
