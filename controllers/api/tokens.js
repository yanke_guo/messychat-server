'use strict';

var sha1    = require('sha1');
var _       = require('lodash');
var config  = require('../../config');
var random  = require('../../lib/random');

var tokens = module.exports;

tokens.destroy = function * () {
  var Token = this.Token;
  var token = this.currentToken;
  var token_id = this.params.token_id;
  if (token_id == 'current') {
    token_id = token._id;
  }
  if (token_id == token._id) {
    this.currentUser.apn_token = null;
    this.currentUser.save();
    this.session.token = null;
  }
  Token.findOneAndRemove({ user_id: this.currentUser._id, _id: token_id });
  this.status = 200;
  this.body = {};
};

tokens.create = function * () {
  var Auth = this.Auth;
  var User = this.User;
  var Token= this.Token;
  var authParams = this.authParams;

  var auth = yield Auth.findOne(authParams).exec();
  if (auth) {
    var user = yield User.findById(auth.user_id).exec();
    var token= yield Token.create({ token: random(64), user_id: user._id });

    if (user.apn_token) {
      var that = this;
      setTimeout(function(){
        var conn = that.apn;
        var msg  = new that.ApnMessage();
        msg.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now
        msg.sound = "ping.aiff";
        msg.alert = "Welcome Back !"
        conn.pushNotification(msg, user.apn_token);
      }, 10000);
    }

    this.session.token = token.token;
    this.status = 200;
    this.body = { user: user.toJSON(), token: token.toJSON() };
  } else {
    this.error(105, 'auth not found');
  }
};
