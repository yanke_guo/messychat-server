'use strict';

/*
 * This concern is currently not used, because IP-based rate limit is kind of irreasonable
 */

var koaLimit=   require('koa-ratelimit');
var redis   =   require('redis');
var config  =   require('../../config');

var redisClient = redis.createClient(config.redis.port, config.redis.host);

module.exports = function(duration, max) {
  return koaLimit({ db: redisClient, duration: duration * 1000, max: max });
};
