'use strict';

var wait = require('../../lib/wait');

/*
 * This concern is used for create a redis-based lock system to ensure user cannot execute some kind of api simultaneously 
 * 
 * Use after needLogin and redis service, which expose a lock function at this
 *
 */

var coLock = function(lockFunc, key) {
  return function(callback) {
    lockFunc(key, function(done) {
      callback(null, done);
    });
  };
};

module.exports = function(lockType) {
  return function * (next) {
    var key = "mclock." + this.currentUser._id.toString() + lockType;
    var done = yield coLock(this.lock, key);
    yield next;
    done();
  };
};
