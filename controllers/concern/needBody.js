'use strict';

var _ = require('lodash');

/*
 * This concern is used for make sure this.request.body exists for certain APIs.
 *
 * Use after apiExtend
 */

module.exports = function (){
  var list = _.compact(_.toArray(arguments));
  return function * (next) {
    if (!this.request.body) {    
      return this.error(-2, 'need request body');
    }
    for (var i = 0; i < list.length ; i++ ) {
      var key = list[i];
      if (this.request.body[key] == null || this.request.body[key] == '') { 
        return this.error(-2, key + ' field is missing');
      }
    }
    yield next;
  };
};
