'use strict';

/*
 * This concern is used for expose some needed function to commonly use in APIs. Mainly is this.error()
 */

var _   =   require('lodash');

module.exports = function() {
  var ext = {
    error : function(code,message) {
              if (code == null) {
                this.status = 500;
                this.body = { code: 888, message: 'Server Internal Error' };
              } else {
                this.status = 400;
                this.body   = { code: code, message: message };
              }
            }
  };
  return function*(next) {
    _.extend(this, ext);
    yield next;
  };
};
