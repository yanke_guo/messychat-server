'use strict';

/*
 * This concern is for validate logined user, using session based token 
 * or `Authorization` header formated as `messy XXXXXXXXX`
 *
 * Use after apiExtend
 *
 */

module.exports = function () {
  return function * (next) {
    var Token = this.Token;
    var User  = this.User;

    var tokenValue = this.session.token;
    if (!tokenValue) {
      var ahead = this.get('Authorization');
      if (ahead) {
        tokenValue = ahead.split(' ')[1];
      }
    }
    if (tokenValue) this.currentToken = yield Token.findOne({token: tokenValue}).exec();
    if (this.currentToken) this.currentUser  = yield User.findById(this.currentToken.user_id).exec();
    if (this.currentUser)  { 
      yield next;
    } else {
      this.session.token = null;
      this.error(-1, "not logined");
    }
  };
};
