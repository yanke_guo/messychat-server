'use strict';

var _       = require('lodash');
var sha1    = require('sha1');
var vali    = require('validator');

var PROVIDERS   =   ['email','facebook'];

// Options
// {
//      email:    boolean,  support email auth, default false
//      facebook: boolean,  support facebook auth, default false
// }

// After API Extend
module.exports = function(options) {
  options = options || {}
  return function * (next) {
    var params  = _.pick(this.request.body, 'provider', 'identifier', 'secret');
    if (_.isString(params.provider)) {
      if (_.contains(PROVIDERS, params.provider) && options[params.provider]) {
        if (params.provider == 'email') {
          if (!vali.isEmail(params.identifier))     return this.error(102, 'email is in wrong format');
          if (!vali.isLength(params.secret, 4, 20)) return this.error(103, 'password is in wrong format, length must greater than 4 and less than 20');
          params.secret = sha1(params.secret);
          this.authParams = params;
          yield next;
        } else {
          if (!_.isString(params.identifier)) return this.error(102, 'no facebook uid is provided');
          if (!params.secret) params.secret = '';
          this.authParams = params;
          yield next;
        }
      } else {
        this.error(101, '[provider] not permited');
      }
    } else {
      this.error(101, '[provider] not provided');
    }
  };
};
