'use strict';

var koa         = require('koa');                   //
var koaStatic   = require('koa-static');            //
var koaLogger   = require('koa-logger');            //
var koaSession  = require('koa-generic-session');   //
var koaRedis    = require('koa-redis');             //
var koaFavicon  = require('koa-favicon');           //
var koaBody     = require('koa-body');              //
var koaJade     = require('koa-jade');              //
var koaOverride = require('koa-methodoverride');    //
var koaEtag     = require('koa-etag');              //
var koaGzip     = require('koa-gzip');              //

var messyServices   = require('./services');        //
var messyControllers= require('./controllers');     //

var config      = require('./config');

var app = koa();

app.name    = 'messychat-server'
app.keys    = ['messychat-server','keys'];
app.proxy   = true;
app.port    = config.server.port;

app.use(koaFavicon(__dirname + '/public/favicon.ico'));
app.use(koaLogger());
app.use(koaGzip());
app.use(koaEtag());
app.use(koaStatic(__dirname + '/public'));
app.use(koaSession({ 
  store: koaRedis({ 
           host: config.redis.host,
           port: config.redis.port
         }) 
}));
app.use(koaBody());
app.use(koaOverride('_method'));
app.use(koaJade.middleware({ 
  viewPath: __dirname + '/views' ,
  debug: true,
  pretty: true,
  compileDebug: true
}));

messyServices(app);
messyControllers(app);

app.on('error',function(err) {
  console.log(err, err.stack);
});

module.exports = app;
