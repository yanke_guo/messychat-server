'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;
var _           = require('lodash');
var config      = require('../config');

mongoose.connect(config.mongoUrl);

/////////   Message Model

var MessageSchema = new Schema({
  user_id   : { type: Schema.Types.ObjectId, index: true},
  content   : String,
  location  : { type: [Number],  index: '2d' },
  created_at: Number
});

var Message = mongoose.model('Message', MessageSchema);

/////////   User Model

var UserSchema = new Schema({
  nickname  : String ,
  location  : { type: [Number],  index: '2d' } ,
  apn_token : String ,
  avatar_url: String
});

var User = mongoose.model('User',UserSchema);

/////////   Token Model

var TokenSchema = new Schema({
  token     : { type: String, index: true,  unique: true },
  user_id   : { type: Schema.Types.ObjectId, index: true }
});

var Token = mongoose.model('Token', TokenSchema);

////////    Auth Model

var AuthSchema = new Schema({
  user_id   : { type: Schema.Types.ObjectId, index: true},
  provider  : { type: String, index: true },
  identifier: { type: String, index: true },
  secret    : String
});

AuthSchema.index({ provider: 1, identifier: -1 }, { unique: true });
AuthSchema.index({ provider: 1, user_id: -1    }, { unique: true });

var Auth = mongoose.model('Auth', AuthSchema);

// Composed Model

var models = {
  Message   : Message,
  User      : User,
  Token     : Token,
  Auth      : Auth
}

module.exports = function(app) {

  app.use(function *(next){
    _.extend(this, models);
    yield next;
  });

};
