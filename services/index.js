'use strict';

var mongo = require('./mongo');
var redis = require('./redis');
var apn   = require('./apn');

module.exports = function(app) {
  redis(app);
  mongo(app);
  apn(app);
};
