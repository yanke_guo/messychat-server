'use strict';

var redis       = require('redis');
var config      = require('../config').redis;
var redisLock   = require('redis-lock');
var coRedis     = require('co-redis');

var redisClient = redis.createClient(config.port, config.host)
var lock            = redisLock(redisClient);
var coRedisClient   = coRedis(redisClient);

module.exports = function(app) {
  app.use(function *(next) {
    this.redis  = redisClient;
    this.lock   = lock;
    this.coredis= coRedisClient;
    yield next
  });
};
