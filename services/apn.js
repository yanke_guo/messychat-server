'use strict';

var apn    = require('apn');
var config = require('../config').apn;

var apnConn= new apn.Connection(config);

//  Expose APN Connection to koa context;

module.exports = function(app) {
  app.use(function * (next) {
    this.apn = apnConn;
    this.ApnMessage = apn.Notification;
    yield next;
  });
};
